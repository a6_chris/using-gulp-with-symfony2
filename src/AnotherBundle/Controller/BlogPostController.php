<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlogPostController extends Controller
{
    /**
     * @Route("/", name="blog")
     */
    public function createAction()
    {
        $em = $this->getDoctrine()->getManager();
        $blogPosts = $em->getRepository('AppBundle:BlogPost')->findAll();

        return $this->render('blog/post.html.twig', [
            'blogPosts' => $blogPosts,
        ]);
    }
}
