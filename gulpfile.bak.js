var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var flatten = require('gulp-flatten');
var minifyCss = require('gulp-minify-css');
var concatCss = require('gulp-concat-css');


var jsPath = './web/js';
var cssPath = './web/css';


gulp.task('internal-sass', function () {
    gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concatCss('./build/internal.css'))
        .pipe(minifyCss())
        .pipe(flatten())
        .pipe(gulp.dest(cssPath))
    ;
});


gulp.task('internal-js', function () {
    gulp.src('./src/**/*.js')
        .pipe(concat('./build/internal.js'))
        .pipe(uglify())
        .pipe(flatten())
        .pipe(gulp.dest(jsPath))
    ;
});


gulp.task('vendor-css', function () {
    gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.css',
        './bower_components/bootstrap/dist/css/bootstrap-theme.css'
    ])
    .pipe(concatCss('./build/vendor.css'))
    .pipe(minifyCss())
    .pipe(flatten())
    .pipe(gulp.dest(cssPath))
    ;
});


gulp.task('vendor-js', function () {
    gulp.src([
        './bower_components/bootstrap/dist/js/bootstrap.js',
        './bower_components/jquery/dist/jquery.js'
    ])
    .pipe(concat('./build/vendor.js'))
    .pipe(uglify())
    .pipe(flatten())
    .pipe(gulp.dest(jsPath))
    ;
});






gulp.task('watch', function () {
   gulp.watch('./src/**/*.scss', ['internal-sass']);
   gulp.watch('./src/**/*.js', ['internal-js']);
});


gulp.task('default', [
    'internal-sass',
    'internal-js',
    'vendor-css',
    'vendor-js',
    'watch'
]);